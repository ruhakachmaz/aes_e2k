#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include "rijndael-alg-fst.h"

/* длина ключа - 16 (128 бит), 24 (192 бита) или 32 (256 бит) */
#define KEYLENGTH 16

/* размер блока - фиксирован в AES, составляет 16 байт */
#define BLOCKSIZE  16  

void AES_Encrypt(const unsigned char *in, unsigned char *out, unsigned int rk[], int nrounds) {
    rijndaelEncrypt(rk, nrounds, in, out);
}

void AES_Decrypt(const unsigned char *in, unsigned char *out, unsigned int rk[], int nrounds) {
    rijndaelDecrypt(rk, nrounds, in, out);
}

int main() {
    unsigned char key[KEYLENGTH];
    unsigned char in[BLOCKSIZE], out[BLOCKSIZE], decrypted[BLOCKSIZE];
    unsigned int rk[4 * (RIJNDAEL_MAXNR + 1)];
    unsigned int drk[4 * (RIJNDAEL_MAXNR + 1)];
    int nrounds;
    struct timeval start, end;
    long seconds, useconds;    
    double mtime;

    /* Инициализируем ключ и исходные данные */
    const char *text = "sometext";
    memcpy(in, text, BLOCKSIZE);

    /* Заполним ключ случайными данными */
    for (int i = 0; i < KEYLENGTH; i++) key[i] = rand() & 0xFF;

    /* Инициализация алгоритма */
    nrounds = rijndaelKeySetupEnc(rk, key, KEYLENGTH * 8);
    rijndaelKeySetupDec(drk, key, KEYLENGTH * 8);

    /* Шифрование данных */
    AES_Encrypt(in, out, rk, nrounds);

    /* Расшифровываем данные */
    AES_Decrypt(out, decrypted, drk, nrounds);

    /* Проверяем, совпадают ли исходные и расшифрованные данные */
    if (memcmp(in, decrypted, BLOCKSIZE) == 0) {
        printf("Текст после шифрования и расшифрования совпадает.\n");
    } else {
        printf("Текст после расшифрования не совпадает с исходным.\n");
    }
    
    /* Выводим зашифрованные и расшифрованные данные */
    printf("Исходный текст:\t");
    for (int i = 0; i < BLOCKSIZE; i++) printf("%c", in[i]);
    printf("\n");

    printf("Зашифрованный текст:\t");
    for (int i = 0; i < BLOCKSIZE; i++) printf("%02x", out[i]);
    printf("\n");

    printf("Расшифрованный текст:\t");
    for (int i = 0; i < BLOCKSIZE; i++) printf("%c", decrypted[i]);
    printf("\n");

    /* Замер времени шифрования и расшифрования */
    gettimeofday(&start, NULL);
    for (long i = 0; i < 1000000; i++) {
        AES_Encrypt(in, out, rk, nrounds);
        AES_Decrypt(out, decrypted, drk, nrounds);
    }
    gettimeofday(&end, NULL);

    seconds  = end.tv_sec  - start.tv_sec;
    useconds = end.tv_usec - start.tv_usec;
    mtime = ((seconds) * 1000 + useconds / 1000.0) + 0.5;

    /* Выводим время затраченное на шифрование и расшифрование */
    printf("Шифрование и расшифрование заняли %f миллисекунд\n", mtime);

    return 0;
}
